﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Web.Http;
using WebApplication1.Helpers;
using System.Diagnostics;
using System.Threading.Tasks;

namespace WebApplication1.Controllers
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }
    }

    public class SaveRecords
    {
        public int Min { get; set; }
        public int Max { get; set; }
    }
    public class ValuesController : ApiController
    {
        // GET api/values


        int min = 1;
        int max = 5000;
        private readonly IDatabase _redisCacheClient;
        private readonly Random _random = new Random();
        LoggerManager _logger;

        public ValuesController()
        {
            _redisCacheClient = RedisConnectorHelper.Connection.GetDatabase();
            _logger = new LoggerManager();
        }

        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public async System.Threading.Tasks.Task<string> GetAsync(int id)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            bool isExists = await _redisCacheClient.KeyExistsAsync($"Product{id}");
            if (isExists)
            {
                var listofkeys1 = await _redisCacheClient.StringGetAsync($"Product{id}");
                _logger.LogInfo(listofkeys1.ToJson());
                return listofkeys1;
            }
            sw.Stop();
            _logger.LogInfo($"GetAsync Elapsed={sw.Elapsed}");
            return "value";
        }

        // POST api/values
        public async System.Threading.Tasks.Task PostAsync([FromBody] Product value)
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            bool isAdded = await _redisCacheClient.SetAddAsync("Product", value.ToJson());
            _redisCacheClient.KeyExpire("Product" , new TimeSpan(1, 0, 30));

            sw.Stop();
            _logger.LogInfo($"Elapsed={sw.Elapsed}");


        }

        //[Route("customers/{customerId}/orders")]
        //public async System.Threading.Tasks.Task PostAsync([FromBody] Product value)
        //{

        //    Stopwatch sw = new Stopwatch();
        //    sw.Start();
        //    bool isAdded = await _redisCacheClient.SetAddAsync("Product", value.ToJson());
        //    sw.Stop();
        //    _logger.LogInfo($"Elapsed={sw.Elapsed}");


        //}

        [HttpPost]
        [Route("Load50Records")]
        public async Task Load50Records()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
           // List<Tuple<string, Product>> list = new List<Tuple<string, Product>>();
            for (int i = 0; i < 50; i++)
            {
                int id = _random.Next(min, max);
                Product p = new Product
                {
                    Id = id,
                    Name = "Product_" + id,
                    Category = "Category"
                };

                bool isAdded = await _redisCacheClient.SetAddAsync("Product" + id, p.ToJson());
                _redisCacheClient.KeyExpire("Product" + id, new TimeSpan(1, 0, 30));



            }
            sw.Stop();
            _logger.LogInfo($"Load50Records - Elapsed={sw.Elapsed}");
        }

        [HttpPost]
        [Route("LoadRecords")]
        public async Task LoadRecords(SaveRecords saveRecords)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
           // List<Tuple<string, Product>> list = new List<Tuple<string, Product>>();
            for (int i = saveRecords.Min; i < saveRecords.Max; i++)
            {
                // int id = _random.Next(min, max);
                Product p = new Product
                {
                    Id = i,
                    Name = "Product_" + i,
                    Category = "Category"
                };
              //  list.Add(new Tuple<string, Product>("Product" + i, p));

                bool isAdded = await _redisCacheClient.SetAddAsync("Product" + i, p.ToJson());
                _redisCacheClient.KeyExpire("Product" + i, new TimeSpan(1, 0, 30));

            }
            sw.Stop();
            _logger.LogInfo($"LoadRecords - Elapsed={sw.Elapsed}");

        }

        // PUT api/<ValuesController>/5
       
        public async Task PutAsync(int id, [FromBody] string value)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            bool isExists = await _redisCacheClient.KeyExistsAsync($"Product{id}");
            if (isExists)
            {
                var product = await _redisCacheClient.StringGetAsync($"Product{id}", StackExchange.Redis.CommandFlags.None);
               
                bool isAdded = await _redisCacheClient.SetAddAsync($"Product{ id}",product);
                _redisCacheClient.KeyExpire("Product" + id, new TimeSpan(1, 0, 30));

            }
            sw.Stop();
            _logger.LogInfo($"PutAsync Elapsed={sw.Elapsed}");

        }

       

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
