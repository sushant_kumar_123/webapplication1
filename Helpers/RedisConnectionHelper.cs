﻿using StackExchange.Redis;
using System;
using System.Configuration;

namespace WebApplication1.Helpers
{
    public class RedisConnectorHelper
    {
        public static string _redisConnectionVal = ConfigurationManager.AppSettings["RedisConnection"];


        static RedisConnectorHelper()
        {
            try
            {
                ConfigurationOptions configurationOptions = new ConfigurationOptions
                {
                    AbortOnConnectFail = false,
                    EndPoints = { _redisConnectionVal }
                };
                RedisConnectorHelper.lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
                {
                    return ConnectionMultiplexer.Connect(configurationOptions);
                });
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred", ex);
            }
        }

        private static Lazy<ConnectionMultiplexer> lazyConnection;

        public static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
    }
}
